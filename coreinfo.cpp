/*
    *
    * This file is a part of CoreInfo.
    * A file information viewer for C Suite.
    * Copyright 2019 CuboCore Group
    *

    *
    * This program is free software; you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation; either version 2 of the License, or
    * (at your option) any later version.
    *

    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *

    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, write to the Free Software
    * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
    * MA 02110-1301, USA.
    *
*/

#include <QFileDialog>
#include <QCloseEvent>
#include <QScroller>
#include <QFileInfo>
#include <QDir>

#include <cprime/themefunc.h>
#include <cprime/messageengine.h>
#include <cprime/variables.h>
#include <cprime/sortfunc.h>
#include <cprime/filefunc.h>
#include <cprime/activitesmanage.h>

#include <ZenLib/Ztring.h>
#include <MediaInfo/MediaInfoList.h>

#include "settings.h"
#include "coreinfo.h"
#include "ui_coreinfo.h"

#define wstring2QString(_DATA) QString::fromUtf8(ZenLib::Ztring(_DATA).To_UTF8().c_str())
#define QString2wstring(_DATA) ZenLib::Ztring().From_UTF8(_DATA.toUtf8())


coreinfo::coreinfo(QWidget *parent)
	: QWidget(parent)
	, ui(new Ui::coreinfo)
	, smi(new settings)
{
	ui->setupUi(this);

	QPalette pltt(palette());
	pltt.setColor(QPalette::Base, Qt::transparent);
	setPalette(pltt);

	loadSettings();
	startSetup();
}

coreinfo::~coreinfo()
{
	delete smi;
	delete ui;
}

/**
 * @brief Setup ui elements
 */
void coreinfo::startSetup()
{
	this->resize(800, 500);

    if (uiMode == 2) {
        // setup mobile UI
        this->setWindowState(Qt::WindowMaximized);

        ui->activitiesList->setVisible(false);
    } else {
        // setup desktop or tablet UI

        if(windowMaximized){
            this->setWindowState(Qt::WindowMaximized);
            qDebug() << "window is maximized";
        } else{
            this->resize(windowSize);
        }

        if(activities){
            loadActivities();
        }
    }

	ui->page->setCurrentIndex(0);
	connect(ui->addFiles, &QPushButton::clicked, this, &coreinfo::openFileDialog);
}

/**
 * @brief Loads application settings
 */
void coreinfo::loadSettings()
{
    // get CSuite's settings
    uiMode = smi->getValue("CoreApps", "UIMode");
    activities = smi->getValue("CoreApps", "KeepActivities");

    // get app's settings
    windowSize = smi->getValue("CoreInfo", "WindowSize");
    windowMaximized = smi->getValue("CoreInfo", "WindowMaximized");
}

void coreinfo::loadActivities()
{
	if (not activities) {
		ui->activitiesList->setVisible(false);
		return;
	}

	ui->activitiesList->clear();

	QSettings recentActivity(CPrime::Variables::CC_ActivitiesFilePath(), QSettings::IniFormat);
	QStringList topLevel = recentActivity.childGroups();

	if (topLevel.length()) {
		topLevel = CPrime::SortFunc::sortDate(topLevel, Qt::DescendingOrder);
	}

	QListWidgetItem *item;

	Q_FOREACH (const QString &group, topLevel) {
		recentActivity.beginGroup(group);
		QStringList keys = recentActivity.childKeys();
		keys = CPrime::SortFunc::sortTime(keys, Qt::DescendingOrder, "hh.mm.ss.zzz");

		Q_FOREACH (const QString &key, keys) {
			QStringList value = recentActivity.value(key).toString().split("\t\t\t");

			if (value[0] == "coreinfo") {
				QString filePath = value[1];

				if (not CPrime::FileUtils::exists(filePath)) {
					continue;
				}

				QString baseName = CPrime::FileUtils::baseName(filePath);
				item = new QListWidgetItem(baseName);
				item->setData(Qt::UserRole, filePath);
				item->setToolTip(filePath);
				item->setIcon(CPrime::ThemeFunc::getFileIcon(filePath));
				ui->activitiesList->addItem(item);
			}
		}

		recentActivity.endGroup();
	}
}

void coreinfo::openFiles(QStringList fileNames)
{
	if (fileNames.length() == 0) {
		CPrime::MessageEngine::showMessage("org.cubocore.CoreInfo", "CoreInfo", "No file found",
											 "Please enter some valid file path.");
		return;
	}

	// Check for the file paths whether they exists
	QStringList paths;

	Q_FOREACH (const auto &path, fileNames) {
		QFileInfo fi(path);

		if (fi.exists()) {
			paths.append(fi.absoluteFilePath());
		}
	}

	if (paths.empty()) {
		CPrime::MessageEngine::showMessage("org.cubocore.CoreInfo", "CoreInfo", "Invalid filepaths",
											 "Please enter some valid file path.");
		return;
	}

	// For opening multiple files in different view
	Q_FOREACH (const auto &filepath, paths) {
		QTreeWidget *treeWid = new QTreeWidget;
		treeWid->setColumnCount(2);
		treeWid->setFrameShape(QFrame::NoFrame);
		treeWid->setHeaderLabels({"Key", "Value"});

		FileInfoMap fiMap = getFileInfo(filepath, true);

		Q_FOREACH (const auto &stream, fiMap) {
			auto *treeItem = new QTreeWidgetItem(treeWid, QStringList(stream.first));
			treeWid->addTopLevelItem(treeItem);

			auto keyValue = stream.second;

			Q_FOREACH (const auto &key, keyValue.keys()) {
				treeItem->addChild(new QTreeWidgetItem(treeItem, {key, keyValue.value(key)}));
			}
		}

		qApp->processEvents();

		treeWid->expandAll();
		treeWid->resizeColumnToContents(0);

		ui->widgetLayout->addWidget(treeWid);
	}

	ui->page->setCurrentIndex(1);
	CPrime::ActivitiesManage::saveToActivites("coreinfo", paths);

	this->setWindowTitle(QFileInfo(paths[0]).fileName() + " - CoreInfo");
}

void coreinfo::openFileDialog()
{
	QStringList list = QFileDialog::getOpenFileNames(this, "Add files", QDir::currentPath(), "*");

	if (list.length() > 0) {
		openFiles(list);
	}
}

void coreinfo::on_activitiesList_itemDoubleClicked(QListWidgetItem *item)
{
	// Remove the old widgets from container if exists
	for (int i = 0; i < ui->widgetLayout->count(); i++) {
		QLayoutItem *item2 = ui->widgetLayout->takeAt(i);

		if (item2 && item2->widget()) {
			ui->widgetLayout->removeWidget(item2->widget());
			delete item2->widget();
		}

		delete item2;
	}

	QString filepath = item->data(Qt::UserRole).toString();
	openFiles({filepath});
}

FileInfoMap coreinfo::getFileInfo(const QString &filename, bool moreDetails)
{
	FileInfoMap info;

	MediaInfoLib::MediaInfoList mediaInfoList;
	mediaInfoList.Open(QString2wstring(filename)); /* Opening only one file */

	int filePos = 0; /* Only one file opened */

	// Get all the available streams
	for (int streamKind = MediaInfoLib::Stream_General; streamKind < MediaInfoLib::Stream_Max; streamKind++) {
		auto streamKind_t = static_cast<MediaInfoLib::stream_t>(streamKind);

		// Available streams for the file
		int streamCount = mediaInfoList.Count_Get(filePos, streamKind_t, -1);

		// Stream kind name
		MediaInfoLib::String streamKindText = mediaInfoList.Get(filePos, streamKind_t, 0, __T("StreamKind/String"),
											  MediaInfoLib::Info_Text);

		// For each stream store them
		for (int streamPos = MediaInfoLib::Stream_General; streamPos < streamCount; streamPos++) {
			MediaInfoLib::String streamKindTextExtended = mediaInfoList.Get(filePos, streamKind_t, streamPos,
					__T("StreamKindPos"),
					MediaInfoLib::Info_Text);

			QMap<QString, QString> keyValue;

			// Get all keys values for a stream
			int keysCount = mediaInfoList.Count_Get(filePos, streamKind_t, streamPos);

			for (int keyIndex = 0; keyIndex < keysCount; keyIndex++) {
				if ((moreDetails || mediaInfoList.Get(filePos, streamKind_t, streamPos, keyIndex,
													  MediaInfoLib::Info_Options)[MediaInfoLib::InfoOption_ShowInInform] == __T('Y'))
						and
						(mediaInfoList.Get(filePos, streamKind_t, streamPos, keyIndex, MediaInfoLib::Info_Text) != __T(""))) {

					MediaInfoLib::String value = mediaInfoList.Get(filePos, streamKind_t, streamPos, keyIndex,
												 MediaInfoLib::Info_Text);
					MediaInfoLib::String valueExtended = mediaInfoList.Get(filePos, streamKind_t, streamPos, keyIndex,
														 MediaInfoLib::Info_Measure_Text);

					MediaInfoLib::String keyText = mediaInfoList.Get(filePos, streamKind_t, streamPos, keyIndex,
												   MediaInfoLib::Info_Name_Text);

					if (wstring2QString(keyText).isEmpty()) {
						keyText = mediaInfoList.Get(filePos, streamKind_t, streamPos, keyIndex,
													MediaInfoLib::Info_Name);
					}

					keyValue[wstring2QString(keyText)] = wstring2QString(value) + wstring2QString(valueExtended);
				}
			}

			QString streamName = wstring2QString(streamKindText);

			if (!wstring2QString(streamKindTextExtended).isEmpty()) {
				streamName += " #" + wstring2QString(streamKindTextExtended);
			}

			info.append({streamName, keyValue});
		}
	}

	mediaInfoList.Close();

	return info;
}

void coreinfo::closeEvent(QCloseEvent *cEvent)
{
    qDebug()<< "save window stats"<< this->size() << this->isMaximized();

    smi->setValue("CoreInfo", "WindowSize", this->size());
    smi->setValue("CoreInfo", "WindowMaximized", this->isMaximized());

	cEvent->accept();
}
