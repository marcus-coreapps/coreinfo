/*
    *
    * This file is a part of CoreInfo.
    * A file information viewer for C Suite.
    * Copyright 2019 CuboCore Group
    *

    *
    * This program is free software; you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation; either version 2 of the License, or
    * (at your option) any later version.
    *

    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *

    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, write to the Free Software
    * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
    * MA 02110-1301, USA.
    *
*/

#pragma once

#include <QWidget>
#include <QTreeWidget>
#include <QMap>
#include <QList>

class settings;
class QListWidgetItem;

typedef QList<QPair<QString, QMap<QString, QString>>> FileInfoMap;

namespace Ui {
	class coreinfo;
}

class coreinfo : public QWidget {
	Q_OBJECT

public:
	explicit coreinfo(QWidget *parent = nullptr);
	~coreinfo();

	void openFiles(QStringList fileNames);

protected:
    void closeEvent(QCloseEvent *cEvent) override;

private slots:
	void openFileDialog();
	void on_activitiesList_itemDoubleClicked(QListWidgetItem *item);

private:
    Ui::coreinfo *ui;
    settings *smi;
    int uiMode;
    bool activities, windowMaximized;
    QSize windowSize;

	void startSetup();
	void loadSettings();
	void loadActivities();
	FileInfoMap getFileInfo(const QString &, bool);
};
